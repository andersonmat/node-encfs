
"use strict";

const fs = require('fs'),
      async = require('async');

const BLOCK_SIZE = 20;

var segmentFile = function(path) {

  // open file
  fs.open(path, 'r', function(err, fd) {

    var buf = new Buffer(BLOCK_SIZE);

    var hasHitEnd = false;
    async.until(function() { return hasHitEnd; }, function(callback) {

      fs.read(fd, buf, 0, BLOCK_SIZE, null, function(err, bytesRead, buffer) {
        console.log(err, bytesRead, buffer.toString(undefined, 0, bytesRead));



        if(bytesRead < BLOCK_SIZE) hasHitEnd = true;

        callback(err);
      });

    }, function(err) {
      console.log(err, 'finished');
      fs.close(fd);
    });

  });



// use fs.fstat()
};




// test with local source code file
segmentFile('./testfile.txt');
