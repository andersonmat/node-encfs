




"use strict";

const crypto = require('crypto'),
      zlib   = require('zlib'),
      fs     = require('fs');


var gzipStream = zlib.createGzip();
var encStream = crypto.createCipher('aes-256-cbc', 'test');


gzipStream.write('test 123');
gzipStream.end();


//gzipStream.pipe(encStream).pipe(fs.createWriteStream('./enc'));


encStream.end('testing 123');
encStream.pipe(fs.createWriteStream('./enc'));

var gunzipStream = zlib.createGunzip();
var decStream = crypto.createDecipher('aes-256-cbc', 'test');


fs.createReadStream('./enc')
  .pipe(decStream)
  //.pipe(gunzipStream)
  .on('data', function(data) {
    console.log(data.toString());
  });
