
const _     = require('lodash'),
      path  = require('path'); 

var _files  = [], // incremented ids
    _tree   = {}, // id => id => id => null term
    _blocks = {}; // stored using hash as id


var pathParts = function(location, callback) {
  [path.sep].concat(location.split(path.sep)).forEach(function(piece) {
    if(!piece.length) return;
    callback(piece);
  })
}; 

var getFile = function(location) {

  var parent = _tree, id = undefined;
  pathParts(location, function(part) {
    var _found = Object.keys(parent).some(function(fileKey) {
      if(_files[fileKey].name === part) {
        parent = parent[id = fileKey];
        return true;
      }
    });
  });
  return _files[id];

};

var addFile = function(location, mode, props) {
  
  // parse file path
  var base = path.basename(location),
      dir  = path.dirname(location);

  // generate file object
  var file = _.assign({
    id   : undefined,
    tree : undefined,
    name : base.length? base : dir,
    mode : mode
  }, props || {});

  file.atime = file.mtime = file.ctime = Date.now();

  // add to file array and capture id (file index)
  file.id = _files.push(file) - 1;

  // add to file tree
  var steps  = [path.sep].concat(dir.split(path.sep)),
      parent = _tree;

  // process all path pieces
  var _finished = steps.every(function(step) {
    if(!step.length) return true;   // ignore empty path pieces

    // match some child of current parent object
    return _.some(Object.keys(parent), function(fileId) {
      var _foundFile = _files[fileId].name === step;
      if(_foundFile) parent = parent[fileId];
      return _foundFile;
    });

  });

  // oops, this won't work
  if(!_finished && file.name != path.sep) {
    _files.pop(); return false;
  }

  // add file / directory to tree
  parent[file.id] = file.mode & 0040000? {} : null;
  file.tree = parent[file.id];

};

// var getFullPath = function(fileId) {
// };


///
/// Testing
///
addFile('/', 0040777);
addFile('/test.txt', 00100655);
addFile('/u', 0040777);
addFile('/u/wot', 0040777);
addFile('/u/wot/m8/m8/m8', 0040777);  // this fails, missing parent path
addFile('/u/wot/m8.txt', 00100655);


console.log(getFile('/'));
console.log(getFile('/test.txt'));
console.log(getFile('/u/wot'));

console.log(_files);
console.log(_tree);
console.log(_blocks);
