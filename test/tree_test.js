
const Tree = require('./tree.js'),
      File = require('./file.js');


var testTree  = new Tree(),
    rootDir   = new File(undefined, 0777, true),
    subDir    = new File(undefined, 0777, true),
    aFile     = new File(undefined, 0644),
    otherFile = new File(undefined, 0644);

// test adding
testTree.add('/',               rootDir);
testTree.add('/test',           subDir);
testTree.add('/test/test.txt',  aFile);
testTree.add('/test/testing.txt',  otherFile);

// // get file
// var testFile = testTree.get('/test/test.txt');
// console.log(testFile);

console.log(testTree);

testTree.remove('/test/testing.txt');
console.log('remove', testTree);

testTree.remove('/test');
console.log('remove', testTree);


