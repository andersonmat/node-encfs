//
//  encfs - Encrypted FUSE File System
//


/**

  this file is currently just a test

  do not take anything in here too seriously


*/

"use strict";

const assert = require('assert');
const mkdirp = require('mkdirp');
//const crypto = require('crypto');
const argv   = require('minimist')(process.argv.slice(2));
const path   = require('path');
const f4js   = require('fuse4js');
const fs     = require('fs');
const _      = require('lodash');

const Tree = require('./tree'),
      File = require('./file');

// extract command line parameters
const MOUNT_DIR  = (argv.m || argv.mount),
      REL_DATA   = (argv.d || argv.data),
      DATA_DIR   = path.resolve(__dirname, REL_DATA),
      PASSPHRASE = (argv.p || argv.passphrase);

// verify parameters are present
// TODO: better parameter parsing
assert.ok(MOUNT_DIR,  'You must provide a directory to mount using --mount');
assert.ok(REL_DATA,   'You must specify where to put encfs data using --data');
assert.ok(PASSPHRASE, 'You must provide an encryption seed using --passphrase');

// make sure both directories exist
if(!fs.existsSync(MOUNT_DIR)) mkdirp.sync(MOUNT_DIR);
if(!fs.existsSync(DATA_DIR))  mkdirp.sync(DATA_DIR);

//console.log(crypto.pbkdf2Sync(PASSPHRASE, '', 10000, 256).toString('hex'));


var fileTree = new Tree();

// add root entry
fileTree.add('/',
  new File('0700', true, 0, 0, 0, process.getuid(), process.getgid())
);


console.log(fileTree);


f4js.start(
  MOUNT_DIR, {
    init : function(cb) {
      console.log('init');
      cb(0);
    },
    release : function(path, fh, cb) {
      console.log('release', path, fh);
      cb(0);
    },
    /*
      http://unix.superglobalmegacorp.com/Net2/newsrc/sys/stat.h.html
    */
    getattr : function(path, cb) {

      var file = fileTree.get(path);

      // no file
      if(!file) return cb(-2);

      cb(0, file.stat());
    },
    readdir : function(path, cb) {

      // the file doesn't exist
      if(!fileTree.exists(path)) return cb(-2);

      cb(0, fileTree.getChildren(path).map(function(file) {
        return file.name;
      }));
    },
    open : function(path, flags, cb) {
      console.log('open', path);
      cb(0);
    },
    read : function(path, offset, len, buf, fh, cb) {

      // fs.read(
      //   fs.createReadStream(DATA_DIR path)
      // );

      console.log('read', path, offset, len);
      buf.write(_.fill(new Array(len), 'a').join(''), 0, len);
      cb(len);
    },
    write : function(path, offset, len, buf, fh, cb) {
      console.log('write', path, offset, len);

      console.log('writing:', buf.toString());

      cb(len);
    },
    access : function(fpath, cb) {
      console.log(fpath);
      cb();
    },
    destroy : function(cb) {
      console.log('destroy');
      process.nextTick(cb);
    },
    create : function(fpath, mode, cb) {
      console.log('create', fpath, mode);

      fileTree.add(fpath, new File(mode));


      cb(0);
    },
    setxattr : function(path, name, value, size, a, b, c, cb) {
      console.log("Setxattr called:", path, name, value, size, a, b, c);
      cb(0);
    },
    statfs : function(cb) {
      cb(0, {
          bsize: 1000000,
          frsize: 1000000,
          blocks: 1000000,
          bfree: 1000000,
          bavail: 1000000,
          files: 1000000,
          ffree: 1000000,
          favail: 1000000,
          fsid: 1000000,
          flag: 1000000,
          namemax: 1000000
      });
    }

  }, false, [/* '-o', 'allow_other' */]
);
