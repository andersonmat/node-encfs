//
//  node-encfs
//  Copyright (c) 2015 - Matt Anderson - andersonmat.me
//
"use strict";

const _ = require('lodash');

// const path = require('path');

// pulled from <sys/stat.h>
// (they're strings because jshint bitches about octal)
const FLAGS = {
  S_IFDIR : '0040000',
  S_IFREG : '0100000'
};


/**
  This represents a file on the file system. This object has no understanding of
  "where" it is located, just its own name as well as any properties associated
  with it (permissions, directory, etc).
*/
var File = function(rwe, dir, atime, mtime, ctime, uid, gid) {
  this.name  = undefined;

  // capture file permissions and whether or not it is a directory
  this.rwe   = typeof rwe === 'string'? parseInt(rwe, 8) : rwe;
  this.dir   = dir || false;

  // capture date information
  var _uTime  = new Date();
  this.atime = atime || _uTime;
  this.mtime = mtime || _uTime;
  this.ctime = ctime || _uTime;

  // pull in the user and group id -- default to negative 1 for undef
  this.uid   = uid || process.getuid();
  this.gid   = gid || process.getgid();   // is this dangerous?
};

File.clone = function(other) {
  // clone incoming object and map to new file object
  return _.assign(new File(), _.cloneDeep(other));
};

File.prototype.constructor = File;

/**
  This calculates the full file mode mask used with `stat`. This includes user /
  group / other read, write, and execute permissions as well as the file type.
*/
File.prototype.mode = function() {
  return this.rwe | parseInt((this.dir)? FLAGS.S_IFDIR : FLAGS.S_IFREG, 8);
};


/**
  Returns an object that cooresponds with the defined struct in <sys/stat.h>.
  http://en.wikibooks.org/wiki/C_Programming/POSIX_Reference/sys/stat.h
*/
File.prototype.stat = function() {
  return _.assign({
    mode : this.mode(), // get file mode
    size : this.size()
  }, _.pick(this, [ // extract other properties
    'atime', 'mtime', 'ctime', 'uid', 'gid'
  ]));
};


File.prototype.size = function() {
  return this.dir? 4096 : Math.floor(Math.random() * 100);
};


module.exports = File;
