# encfs block format

## general format
```c
struct encfs_block_base {
  unsigned char version;
  // ...
};
```

## block version 0x0001
This block version uses SHA-256 hashes and encrypts using AES-256 in CBC mode.

```c
struct encfs_block_v1 = {
  char content_hash[16];
  unsigned int meta;
  char enc_iv[16];
  char data[...];
};
```

The meta member is laid out as follows:
```
[ 2 | flags ][ 15 | encrypted length ][ 15 | unencrypted length ]
```
- The first bit of the flag segment is unused
- The second bit is used to indicate the block is compressed (0x40000000)
- The next fifteen bits represent the length of the data in the file after
  the header (0x3fff8000)
- The final fifteen bits represent the length of the unencrypted data. This is
  used to eliminate any trailing padding. (0x00007fff)
