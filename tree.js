"use strict";

// node and other modules
const Path = require('path'),
      _    = require('lodash');


// internal dependencies
const File   = require('./file.js'),
      Blocks = require('./blocks.js');

/**
  This is used to represent a tree structure of file objects.

  Files are maintained by an assigned id when added to the structure. The id
  depends upon their index in the backing array.

  We represent the tree as an array of sub-trees. This allows us to look up a
  specific file's subtree in O(1) time.
*/
var Tree = function() {

  this._files  = {};
  this._tree   = {};
  this._rootId = -1;
  this._nextId = 0;

};

Tree.prototype.constructor = Tree;

/**
  This is a helper method that is used to get the desired name of the file. This
  verifies that a valid name is used even when handling the base directory.
*/
Tree.prototype._getFileName = function(path) {
  return Path.basename(path) || '/';
};

Tree.prototype._getRoot = function() {
  return (this._rootId === -1)? undefined : this._files[this._rootId];
};

/**
  This attempts to find the parent of the file (or directory) at the given path.

  - The ID of the parent directory is returned on success
  - NULL is returned if we attempt to find the parent of the root directory
  - Undefined is returned if we are unable to find the parent directory
*/
Tree.prototype._getParent = function(path) {

  // handle root directory
  if(path === '/') return null;

  // split the passed path into pieces
  var parts = _.initial(_.without(path.split(Path.sep), ''));

  // traverse list of parts attempting to follow parent => child id
  var parentId  = this._getRoot().id; // start with the root
  var pathFound = !parts.length || _.all(parts, function(piece) {

    // attempt to match next path to child
    return _.any(this._tree[parentId], function(childId) {
      if(this._files[childId].name === piece) {
        // capture new parent id
        return (parentId = childId) !== undefined;
      }
    }, this);

  }, this);

  // return parent if found
  return pathFound? parentId : undefined;
};

/**
  This attempts to add a file to the file tree.

  - Verify we have a root directory
    - If the incoming file is not a root dir, exit

  - Find the parent directory of the file
    - If not found, exit

  - Add file to global list
  - Associate file as child to parent
*/
Tree.prototype.add = function(path, file) {

  /*
    TODO:

    Determine if the file exists at this point so there are no duplicate files.

    A few options that I am considering:
      - Update the file object with props, don't touch tree  (any implications?)
      - Update the file object, drop sub-tree                      (unexpected?)
      - Throw an error                                                 (easiest)
  */

  // verify we have a root directory (or we're adding one)
  if(!(!!this._getRoot() || path === '/')) return false;

  // capture and validate parent id
  var parentId = this._getParent(path);
  if(parentId === undefined) return false;

  // force file name from path
  file.name = this._getFileName(path);

  // track file locally
  this._addToFileList(file);

  // verify we are tracking the root directory
  if(file.name === '/') this._rootId = file.id;

  // add file to the file tree
  this._addToTree(file, parentId);

};

/**
  This adds a file to the local file list, updates the passed file object to set
  its `id` field, and returns the id of the newly added file.
*/
Tree.prototype._addToFileList = function(fileObject) {

  // set value
  this._files[this._nextId++] = fileObject;

  // return the index we used
  return (fileObject.id = (this._nextId - 1));

};

/**
  This does two things:
  - associates the child to the parent
  - sets up the child for use
*/
Tree.prototype._addToTree = function(file, parentId) {

  // create entry for file
  this._tree[file.id] = file.dir? [] : null;

  // associate to parent
  if(parentId !== null) this._tree[parentId].push(file.id);

};

Tree.prototype.get = function(path) {

  // handle root
  if(path === '/') return this._getRoot();

  // get and validate parent
  var parentId = this._getParent(path);
  if(parentId === undefined) return undefined;

  // attempt to match file
  var fileName = this._getFileName(path);
  var fileIds = _.filter(this._tree[parentId], function(childId) {
    return (this._files[childId].name === fileName);
  }, this);

  // return data
  return fileIds.length? this._files[fileIds[0]] : undefined;

};

Tree.prototype.exists = function(path) {
  return !!this.get(path);
};

Tree.prototype.getChildren = function(path) {
  var file = this.get(path);

  // no children for invalid file or non-directory
  if(!file || this._tree[file.id] === null) return [];

  // return list of files
  return _.map(this._tree[file.id], function(childId) {
    return this._files[childId];
  }, this);

};

Tree.prototype._removeById = function(id) {

  // recursively remove children
  if(this._tree[id] !== null && this._tree[id] !== undefined)
    this._tree[id].forEach(this._removeById.bind(this));

  // remove file in list
  return (this._files[id] = this._tree[id] = undefined) === undefined;

};

Tree.prototype.remove = function(path) {
  var file = this.get(path);
  return file? this._removeById(file.id) : false;
};

module.exports = Tree;
